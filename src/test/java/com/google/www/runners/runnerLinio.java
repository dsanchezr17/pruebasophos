package com.google.www.runners;

import org.junit.runner.RunWith;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import cucumber.api.CucumberOptions;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features="src/test/resources/com/google/www/features/pruebaLinio.feature",
				glue ="com.google.www.stepdefinitions",
                tags ="@tag" )
public class runnerLinio {

}
