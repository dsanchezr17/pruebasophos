package com.google.www.stepdefinitions;




import static io.restassured.RestAssured.get;

import org.hamcrest.Matchers;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
public class definitionsRest {

	
	
	private String ROOT_URI="https://gorest.co.in";
	
	//token de autenticacion
	private String token="dpoWLaEd2nVa_5dseDTg_Qenijnd5hXO-ALp";
	
	Response response;
	@When("^consumir el servicio rest$")
	public void consumir_el_servicio_rest() throws Exception {
		
		 response = get(ROOT_URI + "/public-api/users/120?_format=json&access-token="+token);
	
	}

	@Then("^validar el nombre \"([^\"]*)\"$")
	public void validar_el_nombre(String nombre) throws Exception {
		response.then().body("result.first_name",Matchers.is(nombre));
		
	}

		
}
