package com.google.www.stepdefinitions;


import com.google.www.questions.carritoComprar;
import com.google.www.questions.productoAgregado;
import com.google.www.tasks.agregarProducto;
import com.google.www.tasks.usuario;

import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import static org.hamcrest.Matchers.equalTo;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static  net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static  net.serenitybdd.screenplay.GivenWhenThen.seeThat;
public class definitionsLinio {

	
	usuario Tasks;
	String name;
	@Before
	public void setTheStapeg() {
		OnStage.setTheStage(new OnlineCast());
	}

	
	@When("^busco el producto \"([^\"]*)\"$")
	public void busco_el_producto(String producto) throws Exception {
	
	   this.name="user";
	   
		theActorCalled(name).attemptsTo(
				
			usuario.buscarProducto(producto)
				);
		
			
		
	}
	
	@When("^agrego el producto al carrito de compras$")
	public void agrego_el_producto_al_carrito_de_compras() throws Exception {
		
		theActorCalled(name).attemptsTo(
				
				agregarProducto.agregaProdcuto()
					);
	}

	@Then("^valido la adicion del producto$")
	public void valido_la_adicion_del_producto() throws Exception {
	    
		
		theActorInTheSpotlight().should(
				seeThat("Pantalla del carrito de compras",carritoComprar.value(),equalTo("CARRITO DE COMPRAS")),
				seeThat("Pantalla del carrito de compras",productoAgregado.value(),equalTo("1"))
				
				);
	}




}

