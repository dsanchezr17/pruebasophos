package com.google.www.tasks;

import static net.serenitybdd.screenplay.Tasks.instrumented;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
public class agregarProducto  implements Task {

	@Override
	public <T extends Actor> void performAs(T actor) {
		
		
		
		actor.attemptsTo(
				
				
				Click.on(linioObject.btnAgregar),
				Click.on(linioObject.btnIrCarrito)
			);
	
		

	}
	public static Performable agregaProdcuto() {
		return instrumented(agregarProducto.class);
	}


}
