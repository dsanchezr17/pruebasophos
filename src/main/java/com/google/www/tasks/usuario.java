package com.google.www.tasks;

import static net.serenitybdd.screenplay.Tasks.instrumented;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actions.Click;

public class usuario implements Task{

	
	
	private final String name_product;
	
	public usuario(String name_product) {
		this.name_product= name_product;
	}
	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Open.url("https://www.linio.com.pe/"));
		actor.attemptsTo(
				
				Click.on(linioObject.txtProducto),
				Enter.theValue(name_product).into(linioObject.txtProducto),
				Click.on(linioObject.btnBuscar),
				Click.on(linioObject.producto_1)		
				
			);
		
		

	}

	
	public static Performable buscarProducto(String name_product) {
		return instrumented(usuario.class,name_product);
	}

		
		

}
