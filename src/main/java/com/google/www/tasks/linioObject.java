package com.google.www.tasks;

import org.openqa.selenium.By;



//https://www.linio.com.pe/
public class linioObject {

	
	
	
	public static By txtProducto = By.xpath("//*[@id='navbar']//*[@class='form-control']");
	public static By btnBuscar = By.xpath("//*[@class='btn btn-primary btn-search']");
	
	
	public static By producto_1 = By.xpath("//*[@id=\"catalogue-product-container\"]/div[1]/a/div[1]");
	public static By txtNombreProdcuto = By.xpath("//*[@class='product-detail row']//div/h1/span[2]");
	public static By btnAgregar = By.xpath("//*[@id='buy-now'][1]");
	public static By btnIrCarrito = By.xpath("//*[@id=\"alert-add-to-cart\"]/div/div/div[4]/a");
	
	//nombre para el assert
	
	public static  By txtCarritoCompras = By.xpath("//*[@class='title-section row']");
	public static By txtCantidadProducto = By.xpath("//*[@class='header-section-profile col-lg-2 col-md-3 header-icons-row']/a[4]/span[2]");

	

}
