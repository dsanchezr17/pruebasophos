package com.google.www.questions;

import com.google.www.tasks.linioObject;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;

public class carritoComprar implements Question<String>{

	
	
	public static  Question<String> value(){
		return new carritoComprar();
	}


	@Override
	public String answeredBy(Actor actor) {
		
		return 	BrowseTheWeb.as(actor).find(linioObject.txtCarritoCompras).getText();
	}

}
